﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    // reference video: https://youtu.be/whzomFgjT50

    public Transform firePoint;
    public GameObject projectilePrefab;

    public float projectileForce = 20f;

    public float shotInterval = 0.5f;
    private float shotTime = 0.0f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1")); // Fire1 is pre-inputed in Unity as mouse1
        {
            if ((Time.time - shotTime) > shotInterval)
            {
                Shoot();
            }
        }
    }

    void Shoot()
    {
        shotTime = Time.time;

        if (Input.GetKey(KeyCode.Mouse0))
        {
            GameObject projectile = Instantiate(projectilePrefab, firePoint.position, Quaternion.identity);
            Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();
            rb.AddForce(firePoint.up * projectileForce, ForceMode2D.Impulse);
        }
    }
}
