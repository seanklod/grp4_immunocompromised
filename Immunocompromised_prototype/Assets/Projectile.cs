﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    // reference video: https://youtu.be/whzomFgjT50

    public GameObject hitEffect; // adds affect to point of collision

    void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.identity);
        Destroy(effect, 5f); // deletes effect with 5 second delay
        Destroy(gameObject); // deletes bullet immediately
    }
}
