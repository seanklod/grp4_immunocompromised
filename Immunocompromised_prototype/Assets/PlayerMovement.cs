﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // reference video: https://youtu.be/whzomFgjT50

    public float moveSpeed = 5f; // player movement speed
    public Animator animator;

    public Rigidbody2D rb; // reference to rigid body (component that moves player)

    Vector2 movement; // stores x and y values from Update function to be use in FixedUpdate function
    
    // Update is called once per frame (based on frame rate)
    void Update()
    {
        // Handles player input
        movement.x = Input.GetAxisRaw("Horizontal"); // gives a value between -1 (left arrow/A key) and 1 (right arrow/D key); 0 (nothing pressed)
        movement.y = Input.GetAxisRaw("Vertical"); // gives a value between -1 (down arrow/S key) and 1 (up arrow/W key); 0 (nothing pressed)

        animator.SetFloat("Speed", movement.x);
    }

    // works similarly to update function, but in a fixed timer (not by frame rate)
    void FixedUpdate()
    {
        // Handles actual character movement
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
    }
}
